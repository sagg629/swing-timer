import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Sergio A Guzman <sagg629@gmail.com> on 17/05/16 09:14 PM.
 */
public class GUITimer {
    private JButton iniciaButton;
    private JPanel panel;
    private JLabel jLabel;

    public GUITimer() {
        iniciaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TimerTask task = new TimerTask() {
                    @Override
                    public void run() {
                        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                        Date date = new Date();
                        System.out.println(dateFormat.format(date));
                        jLabel.setText(dateFormat.format(date));
                    }
                };
                java.util.Timer timer = new Timer();
                timer.schedule(task, 0, 1000);
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("GUITimer");
        frame.setContentPane(new GUITimer().panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
